import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import HomeView from './home';

export default function AppRouter () {
    return (
        <Router>
            <Route path="/" exact component={HomeView} />
        </Router>
    );
}
